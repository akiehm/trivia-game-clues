import numbers
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from .categories import CategoryOut

# Using routers for organization
# See https://fastapi.tiangolo.com/tutorial/bigger-applications/
router = APIRouter()


class ClueOut(BaseModel):
    id: int
    answer: str
    question: str
    value: int
    invalid_count: int
    category: CategoryOut
    canon: bool


class Message(BaseModel):
    message: str


@router.get(
    "/api/clues/{clue_id}",
    response_model=ClueOut,
    responses={404: {"model": Message}},
)
def get_clue(clue_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT c.id, c.answer, c.question, c.value, c.invalid_count,
                    cat.id, cat.title, cat.canon, c.canon
                FROM clues AS c
                INNER JOIN categories AS cat 
                ON (cat.id = c.category_id)
                WHERE c.id = %s
            """,
                [clue_id],
            )
            row = cur.fetchone()
            if row is None:
                response.status_code = status.HTTP_404_NOT_FOUND
                return {"message": "Clue not found"}
            #record = {}
            # for i, column in enumerate(cur.description):
            #     record[column.name] = row[i]
            clue = {
                "id": row[0],
                "answer": row[1],
                "question": row[2],
                "value": row[3],
                "invalid_count": row[4],
                "category": {
                    "id": row[5],
                    "title": row[6],
                    "canon": row[7],
                },
                "canon": row[8]
            }

            return clue